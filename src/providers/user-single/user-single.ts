import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {NativeStorage} from '@ionic-native/native-storage';

/*
  Generated class for the UserSingleProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UserSingleProvider {

  static userSingleton: UserSingleProvider;
  private user = null;
  private nativeStorage: NativeStorage = new NativeStorage;

  private constructor() {
    
    let self = this;
    this.nativeStorage.getItem('user')
        .then(data => self.user = data)
        .catch(error => self.user = 'indefinido');
  }

  static getInstance():UserSingleProvider{
    if(!this.userSingleton)
      this.userSingleton = new UserSingleProvider();
    return this.userSingleton;
  }

  public getUser(){
    return this.user;
  }
  public setUser(user){
    this.user = user;
  }

  public setUserOnStorage(user): Promise<any>{
    return this.nativeStorage.setItem('user', user);
  }

  public removeUser(){
    this.user = null;
    this.nativeStorage.remove('user');
  }

}