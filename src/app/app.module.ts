import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { GoogleMaps } from '@ionic-native/google-maps';
import { NativeStorage } from '@ionic-native/native-storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CadastroPage } from '../pages/cadastro/cadastro';
import { LoginPage } from '../pages/login/login';
import { MenuPage } from '../pages/menu/menu';
import { PerfilPage } from '../pages/perfil/perfil';
import { ProcurarRotaPage } from '../pages/procurar-rota/procurar-rota';
import { CadastrarRotaPage } from '../pages/cadastrar-rota/cadastrar-rota';
import { CarrosPage } from '../pages/carros/carros';
import { CadastrarCarroPage } from '../pages/cadastrar-carro/cadastrar-carro';
import { EditarCarroPage } from '../pages/editar-carro/editar-carro';
import { ListaRotasPage } from '../pages/lista-rotas/lista-rotas';
import { RotaPage } from "../pages/rota/rota";
import { ListaViagensPage } from "../pages/lista-viagens/lista-viagens";
import { ViagemPage } from "../pages/viagem/viagem";
import { UserSingleProvider } from '../providers/user-single/user-single';
import { ConnectionProvider } from '../providers/connection/connection';
import { HttpModule } from "@angular/http";
import { ResultRotasPage } from '../pages/result-rotas/result-rotas';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CadastroPage,
    LoginPage,
    MenuPage,
    PerfilPage,
    ProcurarRotaPage,
    CadastrarRotaPage,
    CarrosPage,
    CadastrarCarroPage,
    EditarCarroPage,
    ListaRotasPage,
    RotaPage,
    ListaViagensPage,
    ViagemPage,
    ResultRotasPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CadastroPage,
    LoginPage,
    MenuPage,
    PerfilPage,
    ProcurarRotaPage,
    CadastrarRotaPage,
    CarrosPage,
    CadastrarCarroPage,
    EditarCarroPage,
    ListaRotasPage,
    RotaPage,
    ListaViagensPage,
    ViagemPage,
    ResultRotasPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GoogleMaps,
    NativeStorage,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConnectionProvider
  ]
})
export class AppModule {}
