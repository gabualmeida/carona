import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { CarrosPage } from '../pages/carros/carros';
import { UserSingleProvider } from "../providers/user-single/user-single";
import { NativeStorage } from "@ionic-native/native-storage";
import { MenuPage } from "../pages/menu/menu";
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, nativeStorage: NativeStorage) {
    UserSingleProvider.getInstance();
    // Okay, so the platform is ready and our plugins are available.
    // Here you can do any higher level native things you might need.
    var self = this;
    nativeStorage.getItem('user')
      .then( data => self.rootPage = MenuPage)
      .catch( error => self.rootPage = HomePage);
    statusBar.styleDefault();
    splashScreen.hide();
  }
}

