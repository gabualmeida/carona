import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';


/**
 * Generated class for the EditarCarroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-editar-carro',
  templateUrl: 'editar-carro.html',
})
export class EditarCarroPage {

  private cadastro: FormGroup;
  veiculo;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private formBuilder: FormBuilder) {
    this.cadastro = this.formBuilder.group({
      marca: ['VW', Validators.required],
      modelo: ['Siena', Validators.required],
      placa: ['JJJ-1234', Validators.required],
      ano: ['2017', Validators.required],
      cor: ['Preto', Validators.required],
      veiculo: ['carro', Validators.required],
      capacidade: ['4', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditarCarroPage');
  }

}
