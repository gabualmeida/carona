import { ListaViagensPage } from './../lista-viagens/lista-viagens';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserSingleProvider } from '../../providers/user-single/user-single';
import {Headers, RequestOptions, Http} from '@angular/http';
import { ConnectionProvider } from '../../providers/connection/connection';

/**
 * Generated class for the ResultRotasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@Component({
  selector: 'page-result-rotas',
  templateUrl: 'result-rotas.html',
})
export class ResultRotasPage {
  rotas;
  map: any;
  caminho;
  res;
  user;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    this.rotas = this.navParams.get('rotas');
    this.caminho = this.navParams.get('caminho');
    this.user = UserSingleProvider.getInstance().getUser();
  }  

  escolher(rota){
    let passageiro = {
      passageiro_id:  this.user.id,
      lat: this.caminho[0].lat,
      long: this.caminho[0].lng,
      pago: false
    }
    this.requestViagem(rota,passageiro).map(res => res.json())
    .subscribe(data =>{
      console.log(data);
      this.navCtrl.push(ListaViagensPage);
    },error =>{
      console.log(error);
    })
  }

  requestViagem(rota,passageiro){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({
      rota_id: rota,
      passageiro: passageiro
    });
    return this.http.post(ConnectionProvider.address + 'criar-viagem', body, options)
  }

}
