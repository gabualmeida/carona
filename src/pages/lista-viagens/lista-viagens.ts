import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Headers, RequestOptions, Http} from '@angular/http';
import { ConnectionProvider } from "../../providers/connection/connection";
import { UserSingleProvider } from '../../providers/user-single/user-single';

/**
 * Generated class for the ListaViagensPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-lista-viagens',
  templateUrl: 'lista-viagens.html',
})
export class ListaViagensPage {

  user;
  viagens_mot;
  viagens_pass;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    this.user = UserSingleProvider.getInstance().getUser();
    let mot;
    if(this.user.CNH){
      mot = true;
    }else{
      mot = false;
    }
    this.requestGetViagens(mot).map(res => res.json())
    .subscribe(data => {
      console.log(data);
      if(mot){
        this.viagens_mot = data.viagens_motorista;
      }
      this.viagens_pass = data.viagens_passageiro;
    }, error =>{
      console.log(error);
    })
  }

  marcarPago(id){
    this.requestMarcarPago(id).map(res => res.json())
    .subscribe(data =>{
      console.log(data);
    },error =>{
      console.log(error);
    })
  }

  markers(viagem){
    let marker = ''
    viagem.passageiro.forEach(passageiro => {
      marker = marker + '&markers=color:blue%7Clabel:P%7C'+passageiro.lat+','+passageiro.long;
    });
    console.log(marker);
    return marker;
  }

  requestMarcarPago(id){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({
      passageiro_id: id
    });
    return this.http.post(ConnectionProvider.address + 'marcar-pago', body, options)
  }

  requestGetViagens(motorista){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    return this.http.get(ConnectionProvider.address + 'lista-viagem/'+ motorista +'/'+ this.user.id, options)
  }

}
