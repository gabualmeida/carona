import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CadastrarCarroPage } from '../cadastrar-carro/cadastrar-carro';
import { EditarCarroPage } from '../editar-carro/editar-carro';
import {Headers, RequestOptions, Http} from '@angular/http';
import { ConnectionProvider } from "../../providers/connection/connection";
import { UserSingleProvider } from '../../providers/user-single/user-single';

/**
 * Generated class for the CarrosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-carros',
  templateUrl: 'carros.html',
})
export class CarrosPage {
  user;
  veiculos = new Array();
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    this.user = UserSingleProvider.getInstance().getUser();
    this.requestGetCarros().map(res => res.json())
    .subscribe(data => {
      console.log(data);
      data.carros.forEach(carro => {
        this.veiculos.push(carro);
      });
    },error => {
      console.log(error);
    })
  }

  cadastro(){
    this.navCtrl.push(CadastrarCarroPage);
  }

  editar(){
    this.navCtrl.push(EditarCarroPage);
  }

  requestGetCarros(){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    return this.http.get(ConnectionProvider.address + 'lista-carro/'+ this.user.id, options)
  }
}
