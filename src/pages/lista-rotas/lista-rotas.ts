import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Headers, RequestOptions, Http} from '@angular/http';
import { ConnectionProvider } from "../../providers/connection/connection";
import { UserSingleProvider } from '../../providers/user-single/user-single';

/**
 * Generated class for the ListaRotasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-lista-rotas',
  templateUrl: 'lista-rotas.html',
})
export class ListaRotasPage {
  user;
  rotas;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    this.user = UserSingleProvider.getInstance().getUser();
    this.requestGetRotas().map(res => res.json())
    .subscribe(data => {
      console.log(data)
      this.rotas = data.rotas;
    },error => {
      console.log(error);
    })
  }

  requestGetRotas(){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    return this.http.get(ConnectionProvider.address + 'lista-rota/'+ this.user.id, options)
  }

}
