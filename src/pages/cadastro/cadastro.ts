import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MenuPage } from '../menu/menu';
import { UserSingleProvider } from '../../providers/user-single/user-single';
import {Headers, RequestOptions, Http} from '@angular/http';
import { ConnectionProvider } from '../../providers/connection/connection';

/**
 * Generated class for the CadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

  private cadastro: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
              private formBuilder: FormBuilder, public http: Http) {
    this.cadastro = this.formBuilder.group({
      senha: ['', Validators.required],
      confirma_senha: ['', Validators.required],
      nome: ['', Validators.required],
      email: ['', Validators.required],
      telefone: ['', Validators.required],
      tipo: ['', Validators.required],
      cpf: ['',Validators.required]
    }, {
        validator: this.matchingPasswords(
          'senha',
          'confirma_senha'
        )
    })  
  }
  
  valid(tipo){
    if(tipo == 'mot'){
      let control: FormControl = new FormControl('',Validators.required)
      this.cadastro.addControl('cnh',control);
    }else{
      this.cadastro.removeControl('cnh');
    }
    console.log(this.cadastro);
  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  cadastrar(){
    this.requestCadastro(this.cadastro.value).map(res => res.json())
    .subscribe(data =>{
      console.log(data);
      UserSingleProvider.getInstance().setUserOnStorage(data.user).then(data => {
        UserSingleProvider.getInstance().setUser(data);
        this.navCtrl.setRoot(MenuPage);
      })
    }, erro => {
      console.log(erro);
    })
  }

  requestCadastro(values){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({
      email: values.email,
      nome: values.nome,
      password: values.senha,
      telefone: values.telefone,
      CNH: values.cnh,
      CPF: values.cpf
    });
    return this.http.post(ConnectionProvider.address + 'cadastrar', body, options)
  }

}
