import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import {Headers, RequestOptions, Http} from '@angular/http';
import { ConnectionProvider } from "../../providers/connection/connection";
import { UserSingleProvider } from '../../providers/user-single/user-single';


/**
 * Generated class for the CadastrarCarroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-cadastrar-carro',
  templateUrl: 'cadastrar-carro.html',
})
export class CadastrarCarroPage {

  private cadastro: FormGroup;
  user;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private formBuilder: FormBuilder, public http: Http) {
    this.user = UserSingleProvider.getInstance().getUser();
    
    this.cadastro = this.formBuilder.group({
      marca: ['', Validators.required],
      modelo: ['', Validators.required],
      placa: ['', Validators.required],
      ano: ['', Validators.required],
      cor: ['', Validators.required],
    });
  }

  cadastrar(){
    this.requestCadastro(this.cadastro.value).map(res => res.json())
    .subscribe(data => {
      console.log(data);
    }, error => {
      console.log(error);
    })
  }

  requestCadastro(values){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({
      marca: values.marca,
      modelo: values.modelo,
      placa: values.placa,
      ano: values.ano,
      cor: values.cor,
      motorista_id: this.user.id
    });
    return this.http.post(ConnectionProvider.address + 'cadastrar-carro', body, options)
  }
}
