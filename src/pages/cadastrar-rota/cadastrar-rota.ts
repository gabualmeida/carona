import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, LatLng, CameraPosition, MarkerOptions, Marker, GoogleMapOptions } from '@ionic-native/google-maps';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import {Headers, RequestOptions, Http} from '@angular/http';
import { ConnectionProvider } from "../../providers/connection/connection";
import { UserSingleProvider } from '../../providers/user-single/user-single';


/**
 * Generated class for the CadastrarRotaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@Component({
  selector: 'page-cadastrar-rota',
  templateUrl: 'cadastrar-rota.html',
})
export class CadastrarRotaPage {
  polyline: any;

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('origem') origemElement;
  @ViewChild('destino') destinoElement;
  map: any;
  marker;
  autocomplete_origem;
  autocomplete_destino;
  input;
  origemTxt;
  destinoTxt;
  origemLatLng = {};
  destinoLatLng = {};
  caminho = new Array();
  user;
  veiculos;
  dias = new Array();
  private cadastro: FormGroup;  

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private googleMaps: GoogleMaps, private formBuilder: FormBuilder,
              public http: Http) {
    this.user = UserSingleProvider.getInstance().getUser();
    this.requestGetCarros().map(res => res.json())
    .subscribe(data => {
      this.veiculos = data.carros;
    },error =>{
      console.log(error);
    })
    this.cadastro = this.formBuilder.group({
      origem: ['', Validators.required],
      destino: ['', Validators.required],
      hora: ['', Validators.required],
      valor: ['', Validators.required],
      veiculo: ['', Validators.required],
      1:[false],
      2:[false],
      3:[false],
      4:[false],
      5:[false],
      6:[false],
      0:[false]
    });
  }

  ngAfterViewInit() {
    this.loadMap();
  }
  loadMap(){
    let latLng = new google.maps.LatLng(-34.9290, 138.6010);
    
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      fullscreenControl: false,
      mapTypeControl: false
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    
    this.autocomplete_origem = new google.maps.places.Autocomplete(this.origemElement._elementRef.nativeElement.firstElementChild);
    this.autocomplete_destino = new google.maps.places.Autocomplete((this.destinoElement._elementRef.nativeElement.firstElementChild));

  }

  onChangeInputOrigem(e){
    this.origemTxt = e.target.value;
    this.setMarkerOrigem(this.map,this.marker,this.destinoLatLng,this);
  }

  onChangeInputDestino(e){
    this.destinoTxt = e.target.value;
    this.setMarkerDestino(this.map,this.marker,this.origemLatLng,this);
  }

  setMarkerOrigem(map,marker,destinoLatLng,self){
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': this.origemTxt}, function(results, status) {
      //preencher campos
      if (status == 'OK') {
        map.setCenter(results[0].geometry.location);
        marker = new google.maps.Marker({
          map: map,
          animation: google.maps.Animation.DROP,
          position: map.getCenter()
        });
        self.origemLatLng = results[0].geometry.location;
        if(destinoLatLng != {}){
          self.startNavigating();
        }
      }
    });
  }

  setMarkerDestino(map,marker,origemLatLng,self){
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': this.destinoTxt}, function(results, status) {
      //preencher campos
      if (status == 'OK') {
        map.setCenter(results[0].geometry.location);
        marker = new google.maps.Marker({
          map: map,
          animation: google.maps.Animation.DROP,
          position: map.getCenter()
        });
        self.destinoLatLng = results[0].geometry.location;
        if(origemLatLng != {}){
          self.startNavigating();
        }
      }
    });
  }

  startNavigating(){
    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;

    directionsDisplay.setMap(this.map);

    directionsService.route({
        origin: this.origemLatLng,
        destination: this.destinoLatLng,
        travelMode: google.maps.TravelMode['DRIVING']
    }, (res, status) => {

        if(status == google.maps.DirectionsStatus.OK){
          // console.log(res);
          this.polyline = res.routes[0].overview_polyline
          res.routes[0].legs[0].steps.forEach(step => {
            step.lat_lngs.forEach(element => {
              this.caminho.push(element.toJSON());
            });
          });
          directionsDisplay.setDirections(res);
        } else {
            console.warn(status);
        }

    });
  }

  salvar(){
    console.log(this.cadastro.value);
    for(let i = 0; i < 7;i ++){
      let dia = this.cadastro.value[i];
      if(dia){
        this.dias.push(i);
      }
    }
    console.log(this.dias);
    this.requestCadastro(this.cadastro.value).map(res => res.json())
    .subscribe(data => {
      console.log(data);
    }, error => {
      console.log(error);
    })
  }

  requestCadastro(values){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({
      carro_id: values.veiculo,
      diaS: this.dias,
      ponto: this.caminho,
      hora: values.hora,
      valor: values.valor,
      motorista_id: this.user.id,
      polyline: this.polyline
    });
    return this.http.post(ConnectionProvider.address + 'cadastrar-rota', body, options)
  }

  requestGetCarros(){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    return this.http.get(ConnectionProvider.address + 'lista-carro/'+ this.user.id, options)
  }
  

}
