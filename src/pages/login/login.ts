import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { MenuPage } from '../menu/menu';
import { UserSingleProvider } from '../../providers/user-single/user-single';
import {Headers, RequestOptions, Http} from '@angular/http';
import { ConnectionProvider } from '../../providers/connection/connection';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private login: FormGroup;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private formBuilder: FormBuilder, public http: Http) {
    this.login = this.formBuilder.group({
      email: ['', Validators.required],
      senha: ['', Validators.required],
    });
  }

  logar(){
    this.requestLogin(this.login.value).map(res => res.json())
    .subscribe(data =>{
      console.log(data);
      if(data.error == false){
        UserSingleProvider.getInstance().setUserOnStorage(data.user).then(user => {
          UserSingleProvider.getInstance().setUser(user);
          this.navCtrl.setRoot(MenuPage);
        })
      }
    }, erro => {
      console.log(erro);
    })
  }

  requestLogin(values){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({
      email: values.email,
      password: values.senha,
    });
    return this.http.post(ConnectionProvider.address + 'login', body, options)
  }

}
