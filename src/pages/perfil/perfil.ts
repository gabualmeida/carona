import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl,Validators } from "@angular/forms";
import { CarrosPage } from '../carros/carros';
import { UserSingleProvider } from '../../providers/user-single/user-single';
import {Headers, RequestOptions, Http} from '@angular/http';
import { ConnectionProvider } from '../../providers/connection/connection';


/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  public perfil: FormGroup;
  user;
  tipo;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private formBuilder: FormBuilder, public http: Http) {
    this.user = UserSingleProvider.getInstance().getUser();
    this.perfil = this.formBuilder.group({
      nome: [this.user.nome, Validators.required],
      telefone: [this.user.telefone, Validators.required],
    })
    if(this.user.CNH){
      this.tipo = 'mot';
      let controlTipo: FormControl = new FormControl('mot',Validators.required);
      this.perfil.addControl('tipo',controlTipo);
      let controlCNH: FormControl = new FormControl(this.user.CNH,Validators.required);
      this.perfil.addControl('cnh',controlCNH);
    }else{
      this.tipo = 'pass';
      let controlTipo: FormControl = new FormControl('pass',Validators.required);
      this.perfil.addControl('tipo',controlTipo);
    }  
  }

  valid(tipo){
    if(tipo == 'mot'){
      let control: FormControl = new FormControl('',Validators.required)
      this.perfil.addControl('cnh',control);
    }else{
      this.perfil.removeControl('cnh');
    }
  }
  
  editar(){
    this.requestCadastro(this.perfil.value).map(res => res.json())
    .subscribe(data => {
      console.log(data);
    },error =>{
      console.log(error);
    })
  }

  moveToVeiculos(){
    this.navCtrl.push(CarrosPage);
  }

  requestCadastro(values){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify({
      nome: values.nome,
      telefone: values.telefone,
      CNH: values.cnh
    });
    return this.http.post(ConnectionProvider.address + 'editar', body, options)
  }
}
