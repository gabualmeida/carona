import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, AlertController, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { PerfilPage } from '../perfil/perfil';
import { ProcurarRotaPage } from '../procurar-rota/procurar-rota';
import { CadastrarRotaPage } from '../cadastrar-rota/cadastrar-rota';
import { ListaRotasPage } from "../lista-rotas/lista-rotas";
import { ListaViagensPage } from "../lista-viagens/lista-viagens";
import { UserSingleProvider } from '../../providers/user-single/user-single';
import {NativeStorage} from '@ionic-native/native-storage';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  rootPage: any = ProcurarRotaPage;
  pages: Array<{ title: string, page: any, icon: string }>;
  user;
  
  constructor(private menu: MenuController, private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,public navCtrl: NavController, private nativeStorage: NativeStorage) {
    this.user = UserSingleProvider.getInstance().getUser();
    console.log(this.user);
    if(this.user.CNH){
      this.pages = [
        { title: 'Perfil', page: PerfilPage,icon: 'contact'  },
        { title: 'Rotas', page: ListaRotasPage,icon: 'md-git-pull-request'  },
        { title: 'Viagens', page: ListaViagensPage,icon: 'md-map'  },      
        { title: 'Procurar carona', page: ProcurarRotaPage, icon: 'md-search' },
        { title: 'Cadastrar rota', page: CadastrarRotaPage,icon: 'md-add'  },
        { title: 'Sair', page: 'logout',icon: 'md-log-out'  },
      ];
    }else{
      this.pages = [
        { title: 'Perfil', page: PerfilPage,icon: 'contact'  },
        { title: 'Viagens', page: ListaViagensPage,icon: 'md-map'  },      
        { title: 'Procurar carona', page: ProcurarRotaPage, icon: 'md-search' },
        { title: 'Sair', page: 'logout',icon: 'md-log-out'  },
      ];
    }
  }


  openPage(page) {
    this.menu.close();
    if(page == 'logout'){
      this.nativeStorage.clear().then(() =>{
        UserSingleProvider.getInstance().removeUser();
        this.navCtrl.setRoot(HomePage);
      })
    }else{
      this.rootPage = page;
    }
  }

}
